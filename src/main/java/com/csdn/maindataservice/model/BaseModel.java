package com.csdn.maindataservice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.Version;

/**
 * @author StoneHkx
 * @ClassName Model
 * @Description TODO
 * @createDate 2020-01-05 11:09
 * @updatePerson
 * @updateDate
 */
public class BaseModel {

    @TableField(exist = false)
    private long size = 10;
    @TableField(exist = false)
    private long current = 1;

    @TableId(type = IdType.AUTO)
    private Long id;
    private String remark;
    private boolean isDelete;
    @Version
    private int version;

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean delete) {
        isDelete = delete;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
