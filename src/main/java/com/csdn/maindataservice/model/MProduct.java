package com.csdn.maindataservice.model;

/**
 * @author StoneHkx
 * @ClassName MProduct
 * @Description TODO
 * @createDate 2020-01-08 23:35
 * @updatePerson
 * @updateDate
 */

public class MProduct  extends BaseModel{
    private Long userId;
    private String productNo;
    private String productName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
