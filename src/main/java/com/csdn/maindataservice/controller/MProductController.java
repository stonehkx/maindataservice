package com.csdn.maindataservice.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.csdn.maindataservice.model.MProduct;
import com.csdn.maindataservice.service.MProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author StoneHkx
 * @ClassName ProductController
 * @Description TODO
 * @createDate 2019-12-31 00:01
 * @updatePerson
 * @updateDate
 */
@RestController
public class MProductController {

    Logger log = LoggerFactory.getLogger(MProductController.class);

    @Autowired
    private MProductService productService;

    @PostMapping("getProduct")
    public Map<String,Object> getProduct(){
        Map<String,Object> map = new HashMap<>();
        map.put("result",true);
        map.put("data","已经取得商品属性");
        System.out.println("调用负载均衡方法成功！");
        return map;
    }

    /*
    * @Description: 商品分页查询
    * @Param: []
    * @return: java.util.Map<java.lang.String,java.lang.Object>
    * @Author: StoneHkx
    * @Date: 2020/1/8 23:39
    */
    @PostMapping("pageProduct")
    public Map<String,Object> pageProduct(HttpServletRequest request, @RequestBody(required = false)MProduct product){
        Map<String,Object> result = new HashMap<>();
        try {
            if(product == null){
                product = new MProduct();
            }
            IPage<MProduct> data = productService.pageProduct(product,request);
            result.put("reslut",true);
            result.put("data",data);
        }catch (Exception e){
            log.error("查询商品异常"+e.getMessage());
            result.put("reslut",false);
            result.put("data",e.getMessage());
        }

        return result;
    }

}
