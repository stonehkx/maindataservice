package com.csdn.maindataservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.csdn.maindataservice.model.MProduct;

import javax.servlet.http.HttpServletRequest;

/**
 * @author StoneHkx
 * @ClassName MProductService
 * @Description TODO
 * @createDate 2020-01-08 23:45
 * @updatePerson
 * @updateDate
 */
public interface MProductService extends IService<MProduct> {
    IPage<MProduct> pageProduct(MProduct product,HttpServletRequest request);
}
