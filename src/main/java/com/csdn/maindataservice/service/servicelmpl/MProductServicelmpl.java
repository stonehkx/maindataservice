package com.csdn.maindataservice.service.servicelmpl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.csdn.maindataservice.mapper.MProductMapper;
import com.csdn.maindataservice.model.MProduct;
import com.csdn.maindataservice.service.MProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author StoneHkx
 * @ClassName MProductServicelmpl
 * @Description TODO
 * @createDate 2020-01-08 23:47
 * @updatePerson
 * @updateDate
 */
@Service
public class MProductServicelmpl extends ServiceImpl<MProductMapper, MProduct> implements MProductService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public IPage<MProduct> pageProduct(MProduct product, HttpServletRequest request) {

        Page<MProduct> page = new Page<>();
        if(StringUtils.isEmpty(product.getCurrent())){
            page.setCurrent(1);
        }else{
            page.setCurrent(product.getCurrent());
        }
        if(StringUtils.isEmpty(product.getSize())){
            page.setSize(10);
        }else{
            page.setSize(product.getSize());
        }

        LambdaQueryWrapper<MProduct> queryWrapper = Wrappers.lambdaQuery();
        if(!StringUtils.isEmpty(product.getProductName())){
            queryWrapper.like(MProduct::getProductName,product.getProductName());
        }
        queryWrapper.eq(MProduct::getIsDelete,false);

        //获取当前登录用户的信息
        String token = request.getHeader("TOKEN");
        String userStr = stringRedisTemplate.opsForValue().get(token);
        Map userInfo = JSONArray.parseObject(userStr,Map.class);
        Integer userId = (Integer) userInfo.get("id");

        queryWrapper.eq(MProduct::getUserId,userId);

        return this.page(page,queryWrapper);
    }
}
