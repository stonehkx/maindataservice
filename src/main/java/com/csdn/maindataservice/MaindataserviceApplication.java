package com.csdn.maindataservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component("com.**")
@MapperScan("com.csdn.maindataservice.mapper")
public class MaindataserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaindataserviceApplication.class, args);
    }

}
