package com.csdn.maindataservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.csdn.maindataservice.model.MProduct;

/**
 * @author StoneHkx
 * @ClassName MproductMapper
 * @Description TODO
 * @createDate 2020-01-08 23:48
 * @updatePerson
 * @updateDate
 */
public interface MProductMapper extends BaseMapper<MProduct> {
}
